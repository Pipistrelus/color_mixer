package com.pointlessapps.colormixer

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

class ListColorsAdapter(private var colors: MutableList<Item>) :
	RecyclerView.Adapter<ListColorsAdapter.DataObjectHolder>() {

	private var clickListener: ((Int) -> Unit)? = null

	inner class DataObjectHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
		val imageRemove: View? = itemView.findViewById(R.id.imageRemove)
		val progress: ProgressBar? = itemView.findViewById(R.id.progress)
		val value: AppCompatTextView? = itemView.findViewById(R.id.textValue)
		val bg: CardView = itemView.findViewById(R.id.bg)

		init {
			if (imageRemove == null) {
				bg.setOnClickListener {
					clickListener?.invoke(-1)
				}
			} else {
				imageRemove.setOnClickListener {
					clickListener?.invoke(adapterPosition)
				}
			}
		}
	}

	init {
		setHasStableIds(true)
	}

	fun setOnClickListener(clickListener: (Int) -> Unit) {
		this.clickListener = clickListener
	}

	override fun getItemId(position: Int) =
		if (getItemViewType(position) != 0) colors[position - 1].hashCode().toLong() else 0

	override fun getItemViewType(position: Int): Int {
		return if (position == 0) 0 else 1
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataObjectHolder {
		return DataObjectHolder(
			LayoutInflater.from(parent.context).inflate(
				if (viewType == 0)
					R.layout.item_color_add
				else R.layout.item_color,
				parent,
				false
			)
		)
	}

	override fun onBindViewHolder(holder: DataObjectHolder, position: Int) {
		if (getItemViewType(position) != 0) {
			if (!colors[position - 1].ready) {
				holder.progress?.visibility = View.VISIBLE
				holder.imageRemove?.visibility = View.GONE
			} else {
				holder.progress?.visibility = View.GONE
				holder.imageRemove?.visibility = View.VISIBLE
			}
			holder.bg.setCardBackgroundColor(Color.rgb(colors[position - 1].r, colors[position - 1].g, colors[position - 1].b))
			holder.value?.text = String.format("%d%%", colors[position - 1].value)
		} else {
			if (colors.find { !it.ready } != null) {
				holder.bg.also { it.isClickable = false }.alpha = 0.5f
			} else {
				holder.bg.also { it.isClickable = true }.alpha = 1f
			}
		}
	}

	override fun getItemCount() = colors.size + 1
}