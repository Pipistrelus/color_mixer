package com.pointlessapps.colormixer

import android.Manifest
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.sqrt
import kotlin.math.pow

class MainActivity : AppCompatActivity() {

	private var colors = mutableListOf<Item>()
	private lateinit var color: Item
	private lateinit var listColorsAdapter: ListColorsAdapter

	private lateinit var numbers: Array<Int?>
	private val output = mutableListOf<Array<Int?>>()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		setSupportActionBar(toolbar)
		setColorsList()
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		menuInflater.inflate(R.menu.menu, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		return if (item.itemId == R.id.buttonNext) {
			goNext()
			true
		} else super.onOptionsItemSelected(item)
	}

	private fun goNext() {
		FragmentCameraDialog().setOnClickListener {
			if (it.ready) {
				color = it
				toolbar.setBackgroundColor(it.color)

				mixColors()
			}
		}.show(supportFragmentManager, "fragment_camera")
	}

	private fun mixColors() {
		numbers = arrayOfNulls(colors.size)
		series(100, 0)
		setColor()
		listColorsAdapter.notifyDataSetChanged()
	}

	private fun series(start: Int, n: Int) {
		if (n == colors.size - 1) {
			numbers[n] = start
			output.add(numbers.clone())
			return
		}

		for (i in start downTo 0) {
			numbers[n] = i
			series(start - i, n + 1)
		}
	}

	private fun setColor() {
		var bestMix: Item? = null
		var minDst = Double.MAX_VALUE
		var best = arrayOfNulls<Int?>(colors.size)
		for (c in output) {
			var colorR = 0
			var colorG = 0
			var colorB = 0
			c.forEachIndexed { i, value ->
				val percent = value!! / 100f
				colors[i].value = value
				colorR += (colors[i].r * percent).toInt()
				colorG += (colors[i].g * percent).toInt()
				colorB += (colors[i].b * percent).toInt()
			}
			val dst = sqrt((colorR - color.r).toDouble().pow(2) + (colorG - color.g).toDouble().pow(2) + (colorB - color.b).toDouble().pow(2))
			if (dst < minDst) {
				minDst = dst
				bestMix = Item(colorR, colorG, colorB)
				best = c
			}
		}
		bestMix?.also {
			bg.setBackgroundColor(bestMix.color)
			for (i in 0 until colors.size) {
				colors[i].value = best[i]!!
			}
		}
	}

	private fun setColorsList() {
		listColorsAdapter = ListColorsAdapter(colors)
		listColorsAdapter.setOnClickListener { pos ->
			if (pos == -1) {
				showCameraFragment()
			} else {
				colors.removeAt(pos - 1)
				listColorsAdapter.notifyDataSetChanged()
			}
		}
		listColors.layoutManager = LinearLayoutManager(applicationContext, RecyclerView.VERTICAL, true)
		listColors.adapter = listColorsAdapter
	}

	private fun showCameraFragment() {
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), 1)
			return
		}

		FragmentCameraDialog().setOnClickListener { item ->
			val find = colors.find { it.id == item.id }
			if (find == null) {
				colors.add(item)
			} else {
				find.set(item)
			}
			listColorsAdapter.notifyDataSetChanged()
		}.show(supportFragmentManager, "fragment_camera")
	}

	override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
		if (requestCode == 1) {
			if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
				showCameraFragment()
			}
		}
		super.onRequestPermissionsResult(requestCode, permissions, grantResults)
	}
}
