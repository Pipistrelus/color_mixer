package com.pointlessapps.colormixer

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.palette.graphics.Palette
import co.infinum.goldeneye.GoldenEye
import co.infinum.goldeneye.InitCallback
import co.infinum.goldeneye.PictureCallback
import co.infinum.goldeneye.config.CameraConfig
import co.infinum.goldeneye.models.Facing
import co.infinum.goldeneye.models.FlashMode
import co.infinum.goldeneye.models.PreviewScale
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_camera.*

class FragmentCameraDialog : BottomSheetDialogFragment() {

	private var clickListener: ((Item) -> Unit)? = null
	private var rootView: ViewGroup? = null

	private lateinit var goldenEye: GoldenEye

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_camera, container, false) as ViewGroup

			setCamera()
			handleClicks()
		}
		return rootView
	}

	private fun handleClicks() {
		rootView!!.findViewById<View>(R.id.textTapToAdd).setOnClickListener {
			val item = Item(Color.parseColor("#ffffff"))
			goldenEye.takePicture(object : PictureCallback() {
				override fun onPictureTaken(picture: Bitmap) {
//					Palette.from(picture).generate { palette ->
//						item.set(palette!!.getDominantColor(Color.parseColor("#ffffff")), true)
//						clickListener?.invoke(item)
//					}
					rootView?.findViewById<AppCompatImageView>(R.id.previewFreeze)
						?.also {
							it.visibility = View.VISIBLE
						}
						?.setImageBitmap(picture)
				}

				override fun onError(t: Throwable) = Unit
			})
		}
	}

	private fun setCamera() {
		if (ContextCompat.checkSelfPermission(
				context!!,
				Manifest.permission.CAMERA
			) == PackageManager.PERMISSION_GRANTED
		) {
			goldenEye = GoldenEye.Builder(activity!!).withAdvancedFeatures().build()
			val backCamera = goldenEye.availableCameras.find { it.facing == Facing.BACK }
			backCamera?.also {
				goldenEye.open(rootView!!.findViewById(R.id.cameraOutput), backCamera, object : InitCallback() {
					override fun onReady(config: CameraConfig) {
						config.previewScale = PreviewScale.AUTO_FILL
						config.pinchToZoomEnabled = false
						config.flashMode = FlashMode.OFF
						super.onReady(config)
					}

					override fun onError(t: Throwable) = Unit
				})
			}
		} else {
//			TODO: show dialog
		}
	}

	fun setOnClickListener(clickListener: (Item) -> Unit): BottomSheetDialogFragment {
		this.clickListener = clickListener
		return this
	}
}
