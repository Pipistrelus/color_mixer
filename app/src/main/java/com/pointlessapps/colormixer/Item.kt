package com.pointlessapps.colormixer

import android.graphics.Color
import java.util.*

class Item(var r: Int, var g: Int, var b: Int) {

	var id = UUID.randomUUID().hashCode()
	var ready = false
	var color = 0
	var value = 0

	init {
		color = Color.rgb(r, g, b)
	}

	constructor(color: Int) : this(Color.red(color), Color.green(color), Color.blue(color)) {
		this.color = color
	}

	fun set(color: Int, ready: Boolean = false) {
		r = Color.red(color)
		g = Color.green(color)
		b = Color.blue(color)
		this.color = color
		this.ready = ready
	}

	fun set(item: Item) {
		r = item.r
		g = item.g
		b = item.b
		color = item.color
		ready = item.ready
		value = item.value
	}
}